-- 对接开元助手
ALTER TABLE `advertisement_info`
ADD COLUMN `platform`  varchar(32) NOT NULL DEFAULT 'QYD' COMMENT '投放平台,轻易贷:QYD;开元助手:KYZS' AFTER `code`;

ALTER TABLE `advertisement_info`
  ADD COLUMN `drop_terminal`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '投放机型,0:全部机型,1:安卓，2:ios' AFTER `drop_object`,
  ADD COLUMN `drop_financial`  tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否购买过理财产品，0:全部，1:买过，2:未买过' AFTER `drop_terminal`;

ALTER TABLE `message_info`
  ADD COLUMN `platform` VARCHAR(32) NOT NULL DEFAULT 'QYD' COMMENT '消息所属平台,所有平台:ALL,轻易贷:QYD;开元助手:KYZS' AFTER `id`;

ALTER TABLE `share_info`
  ADD COLUMN `platform` VARCHAR(32) NOT NULL DEFAULT 'QYD' COMMENT '投放平台,轻易贷:QYD;开元助手:KYZS'  AFTER `share_name`;

ALTER TABLE `message_push_info`
  ADD COLUMN `platform` VARCHAR(32) NULL DEFAULT 'QYD' COMMENT '投放平台,轻易贷:QYD;开元助手:KYZS'AFTER `id`;

ALTER TABLE `message_module`
  ADD COLUMN `platform` VARCHAR(32) NOT NULL DEFAULT 'QYD' COMMENT '投放平台,轻易贷:QYD;开元助手:KYZS' AFTER `id`,
  DROP INDEX `idx_mm_code`,
  ADD INDEX `idx_mm_code` (`code`) USING BTREE,
  DROP INDEX `idx_mm_name`,
  ADD INDEX `idx_mm_name` (`name`) USING BTREE;

ALTER TABLE `message_user_info`
  ADD COLUMN `platform` VARCHAR(32) NOT NULL DEFAULT 'QYD' COMMENT '投放平台,轻易贷:QYD;开元助手:KYZS' AFTER `id`;

ALTER TABLE `message_user_info`
DROP INDEX `uk_user_messgaeInfo_module` ,
ADD INDEX `idx_user_messgaeInfo_module` (`user_id`, `message_info_id`, `module_code`) USING BTREE COMMENT 'uk_user_messgaeInfo_module';




